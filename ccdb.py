#!/usr/bin/env python
# Copyright (c) 2020 European Spallation Source ERIC
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import click
import json
import requests


def to_json(data, pretty_print=False):
    """Convert data to a JSON string"""
    if pretty_print:
        return json.dumps(data, sort_keys=True, indent=4)
    else:
        return json.dumps(data)


def convert_slot_to_ipc(slot):
    ipc = {
        key: value
        for key, value in slot.items()
        if key in ("id", "name", "description", "deviceType")
    }
    for property in slot["properties"]:
        if property["name"] in ("OperatingSystem", "Hostname"):
            ipc[property["name"]] = property["value"]
    ipc["iocs"] = []
    if slot["children"]:
        for child in slot["children"]:
            ipc["iocs"].append({"name": child["name"], "id": child["id"]})
    return ipc


class CCDBInventory:
    def __init__(self, url):
        self._inventory = {"_meta": {"hostvars": {}}, "all": {"hosts": []}}
        self.api_url = url + "/rest"
        self.session = requests.Session()
        self.session.headers.update({"accept": "application/json"})
        self._slots = None
        self._iocs = None

    def request(self, method, *args, **kwargs):
        r"""Sends a request using the session

        :param method: HTTP method
        :param *args: Optional arguments
        :param **kwargs: Optional keyword arguments
        :return: :class:`requests.Response <Response>` object
        """
        r = self.session.request(method, *args, **kwargs)
        r.raise_for_status()
        return r

    def get(self, endpoint, **kwargs):
        r"""Sends a GET request to the given endpoint

        :param endpoint: API endpoint
        :param **kwargs: Optional arguments to be sent in the query string
        :return: :class:`requests.Response <Response>` object
        """
        return self.request("GET", self.api_url + endpoint, params=kwargs)

    def get_slots(self):
        r = self.get("/slots")
        return r.json()["installationSlots"]

    def get_ipcs(self):
        return [
            convert_slot_to_ipc(slot)
            for slot in self.slots
            if slot["deviceType"] == "IPC"
        ]

    @property
    def slots(self):
        """Return all slots"""
        if self._slots is None:
            self._slots = self.get_slots()
        return self._slots

    def get_host_vars(self, host):
        """Return all variables associated to host"""
        vars = {}
        vars["iocs"] = [{"name": ioc["name"]} for ioc in host["iocs"]]
        return vars

    def inventory(self):
        """Return the inventory as a dict"""
        hosts = self.get_ipcs()
        hostvars = {host["Hostname"]: self.get_host_vars(host) for host in hosts}
        all_hosts = list(hostvars.keys())
        self._inventory["_meta"]["hostvars"] = hostvars
        self._inventory["all"]["hosts"] = sorted(all_hosts)
        self._inventory["all"]["children"] = []
        return self._inventory

    def host_vars(self, hostname):
        """Return the host variables as a dict"""
        hosts = self.get_ipcs()
        host = [host for host in hosts if host["Hostname"] == hostname][0]
        return self.get_host_vars(host)


@click.command()
@click.version_option()
@click.option("--list", is_flag=True, help="List all CCDB hosts")
@click.option("--host", help="Only get variables for a specific host")
@click.option(
    "--ccdb-url",
    help="CCDB URL. Override CCDB_URL environment variable. [default: https://ccdb.esss.lu.se]",
    envvar="CCDB_URL",
    default="https://ccdb.esss.lu.se",
)
@click.option(
    "--pretty", is_flag=True, help="Pretty print JSON output [default: False]"
)
def cli(ccdb_url, host, list, pretty):
    """Retrieve CCDB inventory"""
    if ccdb_url is None or not ccdb_url.startswith("http"):
        raise click.UsageError(
            "Invalid CCDB url. If CCDB_URL is not set, you have to pass --ccdb-url."
        )
    ccdb = CCDBInventory(url=ccdb_url)
    result = {}
    if list:
        result = ccdb.inventory()
    elif host:
        result = ccdb.host_vars(host)
    click.echo(to_json(result, pretty_print=pretty))


if __name__ == "__main__":
    cli()
